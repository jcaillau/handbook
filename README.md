# handbook

Participants
- [J.-B. Caillau](http://caillau.perso.math.cnrs.fr) (Université Côte d'Azur)
- [R. Ferretti](http://www.matfis.uniroma3.it/persone/docenti/docenti.php?persona=44) (Università Roma)
- [E. Trélat](https://www.ljll.math.upmc.fr/trelat) (Sorbonne Université)
- [H. Zidani](http://lmi.insa-rouen.fr/9-membres/professeurs/120-hasnaa-zidani.html) (Insa Rouen)

[shareLaTeX link](https://sharelatex.irisa.fr/project/603691d17aece221bb4de0f0)
