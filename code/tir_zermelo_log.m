function tir_zermelo_log
% Probleme de Zermelo avec obstacle (par pénalisation logarithmique)

clear all; format long;

global v L xf alpha ax ay cy threshold; alpha=0.001; threshold=1e-6;
v=1; L=1; xf=20;
cy=0.4;  %ordonnée du centre de l'obstacle elliptique
ax=2; ay=0.1; % axes de l'obstacle elliptique

%% Initialisation de trajectoire passant au-dessus de l'obstacle :
P0tf = [0.245; 0.0903; 4.982];
%% Initialisation de trajectoire passant en dessous de l'obstacle :
%P0tf =[0.2468; 0.0667; 4.991];


% F(P0tf)
% return

%% Calcul de P0,tf
options=optimset('Display','iter','LargeScale','on');
[P0tf,Fval,exitflag]=fsolve(@F,P0tf,options);

exitflag % 1 si la methode converge, -1 sinon

%% Trace de la trajectoire optimale
options = odeset('AbsTol',1e-9,'RelTol',1e-9) ;
[t,z] = ode45(@sys,[0;P0tf(3)],[0;0;P0tf(1);P0tf(2)],options) ;
subplot(221) ; hold on; plot(z(:,1),z(:,2)) ; xlabel('y_1') ; ylabel('y_2');
theta=0:2*pi/100:2*pi; plot(xf/2+ax*cos(theta),cy+ay*sin(theta),'r'); %hold off;
subplot(222) ; hold on; plot(t,acos(z(:,3)./sqrt(z(:,3).^2+z(:,4).^2))); xlabel('t'); ylabel('u') ;
subplot(223) ; hold on; plot(t,z(:,3)) ; xlabel('t') ; ylabel('p_1')
subplot(224) ; hold on; plot(t,z(:,4)) ; xlabel('t') ; ylabel('p_2')

P0tf

%----------------------------------------------------------------------------

function Xzero=F(X)
% Definition de la fonction dont on cherche un zero

global v L xf alpha ax ay cy threshold;
options = odeset('AbsTol',1e-10,'RelTol',1e-10) ;
[t,z] = ode113(@sys,[0;X(3)],[0;0;X(1);X(2)],options) ;
%% sans obstacle :
% Hamend = v*sqrt(z(end,3)^2+z(end,4)^2) + z(end,3)*(3+0.2*z(end,2)*(L-z(end,2))) -1;
%% avec obstacle :
Hamend = v*sqrt(z(end,3)^2+z(end,4)^2) + z(end,3)*(3+0.2*z(end,2)*(L-z(end,2))) + alpha*log(max((z(end,1)-xf/2)^2/ax^2+(z(end,2)-cy)^2/ay^2-1,threshold)) - 1;
Xzero = [ (z(end,1)-xf)/xf
          (z(end,2)-L)/L
          Hamend ];

%----------------------------------------------------------------------------

function zdot=sys(t,z)   % systeme extremal
global v L xf alpha ax ay cy threshold;
% if ((z(2)<0)|(z(2)>L))
% 	c=0; cp=0;                      % pour éviter que la trajectoire s'échappe
% else
	c = 3+0.2*z(2)*(L-z(2));
	cp = 0.2*L-0.4*z(2);
% end

% if ((abs(z(3))>1)|(abs(z(4))>1))
% 	z(3)=max(min(z(3),1),-1); z(4)=max(min(z(4),1),-1); 
% end

zdot = zeros(4,1);
zdot(1) = v*z(3)/sqrt(z(3)^2+z(4)^2)+c;
zdot(2) = v*z(4)/sqrt(z(3)^2+z(4)^2);
%% sans obstacle :
% zdot(3) = 0;
% zdot(4) = -z(3)*cp;
%% avec obstacle :
zdot(3) = -2*alpha*(z(1)-xf/2)/ax^2/(max((z(1)-xf/2)^2/ax^2+(z(2)-cy)^2/ay^2-1,threshold));
zdot(4) = -z(3)*cp - 2*alpha*(z(2)-cy)/ay^2/(max((z(1)-xf/2)^2/ax^2+(z(2)-cy)^2/ay^2-1,threshold));

