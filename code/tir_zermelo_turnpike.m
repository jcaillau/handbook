function tir_zermelo_turnpike
clear all; format long;

global v L xf T;
v=1; L=1; xf=200; 
T=xf/(v+3+0.2*L^2/4); % temps calculé en fonction de la vitesse au centre du canal
%% Initialisation sans obstacle pour xf=300 et v=1 :
X = [xf/2; L/2; 0.25; 0; T];

% F(P0tf)
% return

%% Calcul de P0,tf
options=optimset('Display','iter','LargeScale','on','MaxFunEvals',1000,'TolFun',1e-6,'TolX',1e-6);
[X,Fval,exitflag]=fsolve(@F,X,options);

exitflag % 1 si la methode converge, -1 sinon

%% Trace de la trajectoire optimale
options = odeset('AbsTol',1e-9,'RelTol',1e-9) ;
T=X(5);
[tback,zback] = ode113(@sysback,[0;T/2],X(1:4),options);
[tforw,zforw] = ode113(@sys,[0;T/2],X(1:4),options);
t = [ flipud(T/2-tback); T/2+tforw ];
z = [ flipud(zback); zforw];
subplot(221) ; plot(z(:,1),z(:,2)) ; title('(x,y)') ;
subplot(222) ; plot(t,acos(z(:,3)./sqrt(z(:,3).^2+z(:,4).^2))); title('u(t)') ;
subplot(223) ; plot(t,z(:,3)) ; title('px') ;
subplot(224) ; plot(t,z(:,4)) ; title('py') ;

X

%----------------------------------------------------------------------------

function Xzero=F(X)
% Definition de la fonction dont on cherche un zero

global v L xf T;
options = odeset('AbsTol',1e-10,'RelTol',1e-10) ;
[t,zback] = ode113(@sysback,[0;X(5)/2],X(1:4),options) ;
[t,zforw] = ode113(@sys,[0;X(5)/2],X(1:4),options) ;
Hamend = v*sqrt(zforw(end,3)^2+zforw(end,4)^2) + zforw(end,3)*(3+0.2*zforw(end,2)*(L-zforw(end,2))) - 1;
Xzero = [ zback(end,1)
		  zback(end,2)
		  (zforw(end,1)-xf)/xf
          (zforw(end,2)-L)/L
          Hamend ];

%----------------------------------------------------------------------------

function zdot=sys(t,z)   % systeme extremal
global v L xf;
% if ((z(2)<0)|(z(2)>L))
% 	c=0; cp=0;
% else
	c = 3+0.2*z(2)*(L-z(2));  % pour éviter que la trajectoire s'échappe
	cp = 0.2*L-0.4*z(2);
% end

zdot = zeros(4,1);
zdot(1) = v*z(3)/sqrt(z(3)^2+z(4)^2)+c;
zdot(2) = v*z(4)/sqrt(z(3)^2+z(4)^2);
pscalf = v*sqrt(z(3)^2+z(4)^2) + z(3)*(3+0.2*z(2)*(L-z(2)));
zdot(3) = 0;
zdot(4) = -z(3)*cp;

%----------------------------------------------------------------------------

function zdot=sysback(t,z)

zdot = - sys(t,z);

