%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction and statement of the problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Brief overview}

Optimal control theory has been widely developed since many decades. The theoretical and numerical achievements, motivated by a body of diverse applications in various domains, provide valuable  insights into the
nature of optimal controls and the corresponding optimal trajectories.
In this chapter our objective is to provide to practitioners a guid to get rapidly acquainted with the most powerful but however easy-to-use methods and algorithms to solve efficiently a given nonlinear optimal control problem in finite dimension.

The most intuitive and popular numerical methods for solving an optimal control problem, called \emph{direct methods}, consist of {\em first discretizing then optimizing}. Such approaches have been investigated in a number of contributions (see, e.g., \cite{Betts}).  From the theoretical point of view, the efficiency  of these methods has been established for some classes of control problems (see \cite{BonnansLaurentVarin,Hager2000,GRKF,RossFahroo,SanzSerna}). From the numerical point of view, the direct approach benefits from the tremendous advances  in numerical optimization methods achieved in the last decades. As shown in Section \ref{sec_direct}, the direct approach is extremely easy to implement for general control problems with constraints on the control variable and on the state variable as well. However, in  general, direct methods may provide only locally optimal solutions and may require a fair initialization of the iterative process in optimization algorithms. Also, they may lack of numerical accuracy, or may become computationally demanding in high dimension.

A major breakthrough in optimal control theory were achieved in the 1950’s by Pontryagin’s research group,
who successfully generalized and extended to a general nonlinear optimal control setting the classical Euler-Lagrange and Weierstrass conditions of
the Calculus of Variations. The early optimality conditions, called Pontryagin Maximum principle (in short, PMP), were subsequently strengthened and extended using methods of convex
and non-smooth analysis, or methods of differential geometry (see \cite{Pontryagin_book}). The PMP inspired effective computational schemes, like the shooting method presented in Section \ref{s4}.  As illustrated on some classical examples, the shooting method provides very accurate numerical solutions. The major drawback of this method is that it requires an {\em a priori} knowledge of the structure of the solution as well as a good approximation of the adjoint state.  In particular, shooting methods are not much suitable for control problems in presence of state constraints or at least, are much more difficult to implement in particular if there are many state constraints.

Another major advance in optimal control was achieved by Richard Bellman in the 1950's, who provided a  description of  how the minimum cost depends on initial conditions, as the solution to a Hamilton-Jacobi partial differential equation. The Hamilton-Jacobi approach, when it can be applied, yields a global solution to the optimal control
problem. It also provides the optimal control in a
feedback form suitable for many engineering applications. Despite of these advantages, the 
approach  suffers from some difficulties of computing the solution
to the Hamilton-Jacobi equation in higher dimensions.
The numerical simulations presented in Section \ref{s5} show that the approximation of the HJB equations, even on coarse grids, provides solutions which depict well the qualitative structure of the optimal trajectories and may thus be used to guess the structure of optimal solutions. However,  to get an accurate  computation of the optimal trajectories, an important numerical effort is necessary to approach the solutions of the HJB equations on very fine grids. 

More recently, other global methods have been developed for control problems, such as the optimistic planning (OP) algorithms introduced in Section \ref{s42}. These methods are based on a discretization process in the control space and do not require any discretization of the state space. As a consequence, OP methods are particularly suitable for control problems with low-dimensional control space.  Moreover, these approaches have the potential to be applied to problems where the dynamics and the cost are given by learning models.
A preliminary analysis of the complexity of the OP methods is now well established, but it remains to further develop the approach to make it more accurate, to integrate the knowledge of the structure of trajectories when available and also for a more efficient implementation. 

Throughout the chapter we consider two well known but representative optimal control examples: the Zermelo and the Goddard problem. Both are quite simple but can be complexified and adapted to more intricate models. We use these two examples to illustrate the numerical methods and show how they can be rapidly and efficiently implemented, with the most up-to-date existing solutions. We also show how the various approaches can be combined to give the best of themselves. For instance, HJB approach and direct methods can be used to obtain a rough solution that successfully provides an initial guess for the (more accurate) shooting method. 
We hope that the codes that we provide, which are also available on the web (as indicated further), can serve as templates to readers interested in adapting them to their specific setting.




\paragraph{Notations.} 
%============================================
Throughout the chapter, $\R$ denotes the set of real numbers,  
$\langle \cdot,\cdot\rangle$ and $\|\cdot\|$ denote respectively
the Euclidean inner product and the norm on $\R^N$(for any $N\geq 1$),
$\BB_N=\{x\in\R^N:\ \|x\|\leq 1\}$ is the closed unit ball 
(also denoted $\BB$ if there is no ambiguity) and $\BB(x;r)=x+r\BB$. 
%% 
For any set $S\subseteq\R^N$, $\inte{S}, \overline{S}$, $\partial S$, $\Co S$
denote its interior, closure, boundary, and convex envelope, respectively. \ For any $a,b\in \R$, we define
	$a\bigvee b:= \max(a,b).$
	Similarly, for $a_1,\cdots,a_m\in \R$, we define 
	$ \bigvee_{i=1}^{m}a_i:= \max(a_1,\cdots,a_m).$
The  notation $W^{1,1}([a,b])$ stands for the usual Sobolev space $\{f\in L^1(a,b), f'\in L^1(a,b)\}$.
%
%
Finally, the abbrevation "w.r.t." stands for "with respect to", and "a.e." means "almost everywhere".
 

%-------------------------------------------------
\subsection{Formulation of the optimal control problem}
%------------------------------------------------
Let $d,r\in\N^*$, let $T>0$ be
a fixed final time horizon and let $U$ be a compact subset of $\R^r$ (with $r\geq 1$).
We consider the finite-dimensional control system in $\R^d$ (for $0\leq t<T$)
%\begin{subequations}
\begin{eqnarray}\label{eq:etat}
 & & \dot \bx(s) = f(s,\bx(s), \bu(s)), \ \mbox{a.e.} \ s\in (t,T),
\end{eqnarray}
%\end{subequations}
%
where the control input $\bu:[0,T]\longrightarrow \R^r$ ($r\geq 1$) is a measurable function such that $\bu(s)\in U$ 
for almost every~$s\in [0,T]$. Throughout the paper, we assume that

\medskip

%\begin{align*}

%\begin{indent}
\mbox{\Hyp{0}}
$\quad$ \text{$U$ is a compact subset of $\R^r$.}
%\end{indent}

\medskip

\noindent
We denote by $\cU$ the set of all admissible controls
$$\cU:=\{\bu:[0,T]\longrightarrow \R^r \mbox{ measurable, and }
\bu(s)\in U \mbox{ a.e.}\}.$$
The dynamics $f:[0,T]\times\R^d\times U\converge \R^d$ and 
  the distributed cost $\ell:[0,T]\times\R^d\times U\converge \R$ 
  satisfy: 
%
\begin{align*}
\hspace*{0.5cm} 
\mbox{{\Hyp{1a}}} \hspace*{0.5cm}
\begin{cases}
(i) \quad  f \text{ is continuous on }[0,T]\times\R^d\times U;\\
%
(ii)\quad \text{$x\rightarrow f(s,x,u)$ is locally Lipschitz continuous in the following sense:}\\
\phantom{(ii)\quad}
  %
  \forall R>0,\ \exists k_R\geq 0,\ \forall (x,y)\in (\BB_{d}(0;R))^2,\ \forall (s,u)\in[0,T]\times U \\
  %
  \phantom{(ii)\quad}
  \hspace{1cm}
  \|f(s,x,u)-f(s,y,u)\| \leq k_R \|x-y \|; \\
%
(iii)\  \exists c_f>0\text{ such that }\ \
  \|f(s,x,u)\|\  \leq\ c_f(1+\|x\|) \ \  \forall (s,x,u)\in[0,T]\times \R^d\times U;
\end{cases}\hspace*{3cm}
\end{align*}
and
\begin{align*}
\hspace*{0.5cm} 
\mbox{{\Hyp{1b}}} \hspace*{0.5cm}
\begin{cases}
  (i) \quad  \ell \text{ is continuous on }[0,T]\times\R^d\times U;\\
%ii)\ \ \text{ for every $s\in[0,T]$ and $u\in U$,}\ f(s,\cdot,u)\ \mbox{is of class}\ C^1.\\
(ii)\quad \text{$x\rightarrow \ell(s,x,u)$ is locally Lipschitz continuous in the following sense:}\\
\phantom{(ii)\quad}
  %\hspace{2cm} 
  \forall R>0,\ \exists k_R\geq 0,\ \forall (x,y)\in (\BB_{d}(0;R))^2,\ \forall (s,u)\in[0,T]\times U \\
  %\hspace{3cm}
\phantom{(ii)\quad}
  \hspace{1cm}
  |\ell(s,x,u)-\ell(s,y,u)| \leq k_R \|x-y \|; \\
%
(iii)\  \exists c_\ell>0\text{ such that }\ \   |\ell(s,x,u)|  \leq\ c_\ell(1+\|x\|)\ \  \forall (s,x,u)\in[0,T]\times \R^d\times U.
\end{cases}\hspace*{3cm}
\end{align*}
The assumptions 
$(ii)$-$(iii)$ of \Hyp{1a} guarantee, 
for every $\bu\in \cU$ and $(t,x)\in[0,T]\times\R^d$, 
the existence of an absolutely continuous curve $\bx:[t,T]\to\R^N$
which satisfies \eqref{eq:etat} and the initial data $\bx(t)=x$.
The Gronwall lemma gives
\begin{align}\label{eq:traj_gronwall}
1+ \|\bx(s)\|\leq(1+\|x\|)e^{c_f(s-t)}\qquad\forall s\in[t,T].
\end{align}
%
%

Given any $t\in [0,T]$ and any $x\in\R^d$,
the set of all admissible pairs 
control-and-trajectories starting at $x$ at time $t$ is denoted by
$$
  \XX_{[t,T]}(x):= 
  \{ (\bx,\bu) \in W^{1,1}(t,T)\times\cU\mid 
   \ (\bx,\bu) \mbox{ satisfies \eqref{eq:etat} with } \bx(t)=x \}.
$$

Throughout the chapter, we consider the optimal control problem  
\begin{equation}\left\{\begin{array}{l}
\mbox{minimize } \ 
   \displaystyle\varphi(\bx(T)) + \int_t^T \ell(s,\bx(s),\bu(s))\,ds, \\
 \hspace*{1cm} (\bx,\bu)\in \XX_{[t,T]}(x), 
\\
   \hspace*{1cm} g(\bx(s))\leq 0 \quad \mbox{for } s\in [t,T],\\
 \hspace*{1cm} g_f(\bx(T))\leq 0,
\end{array}\right. \label{eq.Pb_CO}
\end{equation}
with the agreement that $\inf\emptyset=+\infty$. 
The final cost $\varphi:\R^d\to\R$ 
and the constraint functions $g$ and $g_f$ are given functions satisfying:

\medskip

\Hyp{2} $\varphi:\R^d\to \R$ is 
%
locally Lipschitz continuous
and there exists a constant $c_\varphi\geq 0$ such that 
$$|\varphi(y)|\leq c_\varphi(1+\|y\|).$$

\medskip

\Hyp{3} The constraint functions 
$g:\R^d\to \R^m$ and $g_f:\R^d\to \R^r$ are 
%
locally Lipschitz continuous
and there exists a constant $c_g\geq 0$ such that, for every $y\in \R^d$, 
$$|g(y)|+|g_f(y)|\leq c_g(1+\|y\|).$$

\medskip

The so-called augmented control system
\begin{subequations} \label{eq:augmented_state}
\begin{eqnarray}
\dot\bx(s) & = & f(x,\bx(s),\bu(s))\quad \mbox{a.e. } s\in (t,T),\\
\dot\bz(s) & = & -\ell(s,\bx(s),\bu(s))\quad \mbox{a.e. } s\in (t,T),
\end{eqnarray}
\end{subequations}
is usually considered in optimal control theory to recast the Bolza problem in the Mayer form
\begin{equation}\left\{\begin{array}{l}
\mbox{minimize } \ 
   \displaystyle\varphi(\bx(T)) - z(T), \\
 \hspace*{1cm} (\bx,\bu)\in \XX_{[t,T]}(x_0), 
\\
   \hspace*{1cm} g(\bx(s))\leq 0 \quad \mbox{for } s\in [t,T],\\
 \hspace*{1cm} g_f(\bx(T))\leq 0,
\end{array}\right. \label{eq.Pb_CO_Reformulation}
\end{equation}

In what follows, we will {\bf sometimes} assume that the augmented dynamics satisfies the following assumption (convex epigraph):
\medskip

\Hyp{4} For any $s\in [0,T]$ and every $x\in \R^d$, 
  %$f(s,x,U)$ 
$$
  \bigg\{ \left(\begin{array}{c}
   f(s,x,u)\\ -\ell(s,x,u)+\eta \end{array}\right), \ \ u\in U,\ \ -c_\ell(1+\|x\|)+\ell(s,x,u)\leq \eta\leq 0 \bigg\}
  \quad \mbox{is a convex set.}
$$ 
%\fbox{VERIFIER/ADAPTER}

\begin{rem} Note that if $\ell\equiv 0$ (Mayer problem), \Hyp{4} reduces to
  \begin{equation*} \label{eq:H4b}
  \mbox{$f(s,x,U)$ convex for all $(s,x)\in[0,T]\times \R^d$.}
 \end{equation*}
\end{rem}

\begin{rem}\label{rem.existence}
For $0\leq a<b \leq T$ and $(x,z)\in \R^d\times \R$, consider the set of all trajectories satisfying \eqref{eq:augmented_state} on the time interval $[a,b]$, for a control input $u\in \cU$, and starting at a position $(x,z)$ at time $a$:
\begin{eqnarray*}
  \cS_{[a,b]}(x,z):=\{ (\bx,\bz)\in W^{1,1}([a,b])\mid \exists \bu\in \cU
  \ \mbox{ such that } (\bx,\bz,\bu) \mbox{ satisfies } \eqref{eq:augmented_state}, \hspace*{1cm}& & \\
\mbox{ with } \bx(a)=x,\ \bz(a)=z\}. & & 
\end{eqnarray*}

Under assumptions \Hyp{0}, \Hyp{1a}, \Hyp{1b},  by \eqref{eq:traj_gronwall},
the set $\cS_{[a,b]}(x,z)$ is bounded in $W^{1,1}([a,b])$.  Moreover, if \Hyp{4} is satisfied then  $\cS_{[a,b]}(x,z)$ is a compact set in $W^{1,1}([a,b])$ endowed with the $C^0([a,b])$-topology (see
\cite[Theorem 1.4.1]{CELAUB}). 
%
Therefore, 
if there exists  a trajectory $\bx\in \cS_{[0,T]}(x_0,0)$ 
that satisfies the  constraints
$g(\bx(s))\leq 0$ for all $s\in [t,T]$ and $g(\bx(T))\leq 0$,
then the the control problem \eqref{eq.Pb_CO_Reformulation} has an optimal solution.
%%%When assumption \Hyp{4}, there is no guarantee that ${ S}_{[0,T]}(x_0,0)$is a closed set.
%%%In this case,  the 
%%%infimum of the control problem may not be achieved by an admissible trajectory.
%%%In this case, it is usual  to consider the closure of ${ S}_{[0,T]}( x_0,0)$ 
%%%(topologized by the $C^0([t,T])$-norm), see \cite{Barron_Jensen_96, Frankowska_Rampazzo_99}.
%%%For this, one should introduce the convexified set-valued dynamics 
%%%$$
%%%  (f,\ell)^\sharp (t, x):= {\mbox{co}}( \{(f(t, x,u),\ell(t,x,u), \ u\in U\}), \quad \mbox{for}\ t \in [0,T], \ 
%%%      \hat x\in \R^d.
%%%$$
%%%Under assumptions \Hyp{0}-\Hyp{3} the following differential inclusion
%%%admits absolutely continuous solutions in $[t,T]$ (see \cite{Aubin_Cellina})
%%%\begin{subequations}
%%%\label{eq:ysharp}
%%%\begin{eqnarray}
%%% & & \dot \bx(s) \in { f}^\sharp (s, \bx(s)), \quad a.e.\ s\in (t,T).\\
%%% & &  \bx(t)=x,
%%%\end{eqnarray}
%%%\end{subequations}
%%%Let us denote ${S}^\sharp_{[t,T]}(x)$ to be the set of all the solutions to \eqref{eq:ysharp}.
%%%This set is precisely the closure of  ${S_{[t,T]}}^\sharp(x)$ 
%%%in $C^0(0,T)$ (see for instance \cite{Frankowska_Rampazzo_99})
%%%\be\label{eq:closure} 
%%%  { S_{[t,T]}}^\sharp(x) \equiv  \overline{{ S_{[t,T]}(x)}}^{\,C^0}.
%%%\ee
%%%Moreover, by Filippov's theorem  ${S}^\sharp_{[t,T]}(x)$ is a compact subset of $C^0([t,T])$.
%%%Now, define the relaxed control problem, and its associated value function $\VV^\sharp$, as follows
%%%\be\label{eq:varsharp}
%%%  \VV^\sharp(t,x) := 
%%%  \min_{\bx \in {S^\sharp}_{[t,T]}(x)}  \varphi(\bx(T)).
%%%\ee
%%%In this case, the function $\VV^\sharp$ is l.s.c.\ and the minimum in \eqref{eq:varsharp} 
%%%is achieved. If assumption \Hyp{4} is satisfied, then $S_{[t,T]}^\sharp(x) = S_{[t,T]}(x)$ and the value function $\VV$ coinc\"ides with $\VV^\sharp$. In this case,  $\VV$ is also l.s.c.\ and the minimum in \eqref{vdef} 
%%%is achieved
\end{rem}
%

\subsection{Examples} \label{s2}
In this section, we present two well-known examples that we are going to consider throughout. In our opinion, they illustrate nicely the most classical difficulties encountered in theoretical and numerical optimal control, and lend themselves to a number of more complicated variants. They are expected to serve as ``templates" to the reader who aims at getting acquainted with the main issues in numerical optimal control.

\paragraph{Example 1: Zermelo problem} 
A boat with coordinates $\bx(s)=(y_1(s),y_2(s))$ navigates through a canal $\R\times[a,b]$, starting at 
$\bx(0)=x=(x_1,x_2)$. We will either take $a=0$ and $b=1$, or $a=-2$ and $b=2$. One may want to reach a fixed final point, or to reach an island $\cC$,  with minimal cost. The cost function may be an energy, or the final time, or the lateral deport, etc. The control system is 
\begin{subequations}\label{eq:dyn_zermelo}
\begin{eqnarray}
  & & \dot y_1(s) =  v(s) \cos(u(s)) + h(y(s)),\\
  & & \dot y_2 (s) =  v(s) \sin(u(s)),
\end{eqnarray}
\end{subequations}
where $u(s) \in [0,2\pi]$ is the first control (angle), $v(s)\in [0,V_{max}]$ is a second control (speed of the boat), 
and $h(y(s))$ is the current drift (along the $x_1$-axis).
Because of the drift term (which can be greater than $V_{\max}$), the system may not be controllable.  Consider a  target $\cC:=\mathbb{B}(c,r_0)$ that is a   ball with radius $r_0\geq 0$ and centered at a given point $c$ located in the canal.  The target  represented by a function $\psi$ defined by 
$g_f(x):=\|x-c\|-r_0$  as 
$$x\in \cC \Longleftrightarrow g_f(x)\leq 0.$$
Consider also a set of constraints $\cK:=\{x\in \R^2,\ g(x)\leq 0\}$ where $g$ is a given function that is non-positive in a region where the boat can move and $g$ is positive in the location of the obstacles that the boat should avoid.   In this example, the cost function could be the time, or the energy, required to steer the boat  from a given position $x$ to the target $\cC$. 
%

\paragraph{Example 2: Goddard Problem}
We consider the optimal control problem associated with a vertical ascension flight of 
a rocket,  known as Goddard problem. 
 The dynamics of the rocket is  defined with three state variables: $r$, the altitude, $v$ the relative speed  and $m$, the total mass. In general, a  dimensionless version for the motion equations is considered in the literature:
\be
  \dot r & = & v  \label{eq101}\\
  \dot v & = & \frac{1}{m} (T_{max}\, u - D(r,v)) - \frac{1}{r^2} \\
  \dot m & = & - T_{max}\, b\, u \label{eq103}
\ee 
where $D(r,v)$ is the drag force and $T_{max}\cdot u$ is the trust force.
The  dimensionless initial state of the rocket is given by 
$r(0)=1.0$, $v(0)=0$, $m(0)=1$.
Here $r=1$ corresponds to the Earth's ground level and $m=1$ to the initial total mass of the rocket.
The motion of the rocket is controlled 
by the trust factor $u\in[0, 1] $ so that  the trust force is on the interval $T= T_{max}\cdot u\in [0, T_{max}]$.
 
The drag  force is  a nonlinear function of $r$ and $v$. Its expression depends on the choice of a model for the 
atmosphere and  on the structure of the rocket:
$D(r,v)= C_D \rho(r) |v| v$,
where $C_D$ is the drag coefficient of the rocket and $\rho$ is the atmospheric density.  In this section we 
consider  the  case of constant drag coefficient   (it depends in general on the Mach number)  and exponential model of the 
atmospheric density:
$$
  D(r,v)= C_D\ v^2\ e^{-\beta\,(r-1)} .
$$
 This definition of the rocket's model has been widely studied in the literature. We take   
 $C_D=310.0$, $\beta=500.0$, $T_{max}=3.5$, $b=2.0$,
 as in \cite{Tsiotras_Kelley_92} and other references. The corresponding model is then an approximation of real flight conditions for some rockets. 
  The optimal control problem consists of  maximizing the final altitude $r(t_f)$ at a given fixed final time $t_f>0$ under 
the following final  constraint (which represents  a limit on the fuel consumption):
$m(t_f)\ge m^\ast$,
with $m^\ast=0.6$. 
Finally, the optimal control problem is formulated as:
$$
\begin{array}{cclc}
 &  & \inf_{u(.)}\ \bigg(-r(t_f)\bigg) &\\
  & & & \\
   \dot r & = & v  &   r(0)=1\\
  \dot v & = & \displaystyle \frac{1}{m} (T_{max}\, u - D(r,v)) - \frac{1}{r^2} & v(0)=0\\
   & & & \\
  \dot m & = & - T_{max}\, b\, u  & m(0)=1 \\
   & & & \\
  & & m^\ast-m(t_f)\le 0&
\end{array}
  $$
