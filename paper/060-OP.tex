
\section{Optimistic planning algorithms}
\label{s42}
%%%%%%----------%%%%%%%%%



As mentioned previously, direct and indirect methods are quite simple to implement and they provide local solutions with a great precision.  These methods depend  on the initialization -- especially the shooting method  which is particularly sensitive to the initialization and also requires an \emph{a priori} knowledge of the optimal trajectory structure. On the other hand, the HJB approach always provides a global optimum, but it  requires a much greater computational effort because the value function must first be computed in a quite large domain.

The approach that we present here is a global approach based on a  discretization in the {\em space of controls}, combined with \emph{optimistic planning} (OP) algorithms \cite{ref52,ref51}
(without  requiring any discretization of the state space). This approach is interesting especially for applications where the control dimension $r$ is much lower compared to the state dimension $d$.   
On a given discretization of the time interval, our approach will seek to identify the best control strategy to apply on each time sub-interval. The OP methods perform the optimal control search by branch and bound on the control set, always refining the region with the best lower bound of the optimal value (this is what justifies  the term ``optimistic").  An interesting feature of these algorithms is the close relationship between computational resources and quasi-optimality,  which exploits some  ideas of reinforcement learning \cite{mun-2014}. 
Indeed, for  given computational resources,  the OP approaches provide a sub-optimal strategy
whose performance is {\em as close as possible} to the optimal value (with the available resources). 

First, for $N\geq 2$, consider a uniform partition of $[0,T]$ with $N+1$ time steps: $t_k=k\Delta t$, $k=0,\dots,N$,
where $\Delta t=\frac{T}{N}$ is the step size. 
For a sequence of actions $\bu=(u_k)_{0\leq k \leq N-1}\in U^N$, we consider the discrete-time dynamical system
\begin{equation}
  \label{eq:discrete}
\begin{cases}
    y_0=x,\\
    y_{k+1}=F_k(y_k,u_k)\quad k=0,...,N-1,
    \end{cases}
\end{equation}
where $F_k(x,u)$ is an approximation of the solution to the system 
$\dot\bx(s)=f(\bx(s),u)$ on  $(t_k,t_{k+1})$,
 with the initialcondition $\bx(t_k)=x$. More precisely, we assume that 
 $$\left\|F_k(x,u)-x-\int_{t_k}^{t_k+1}f(s,\bx(s),u)\,ds\right\| = \mathrm{O}(\Delta t) \quad \mbox{ for all } x\in \R^d, \ u\in U.$$

Consider also an instantaneous cost function $L_k$ 
that approximates the integral of $\ell$ over an interval $[t_k,t_{k+1}]$, for $k=0,...,N-1$:

$$\left\|L_k(x,u)-\int_{t_k}^{t_k+1}\ell(s,\bx(s),u)\,ds\right\| = \mathrm{O}(\Delta t) \quad \mbox{ for all } x\in \R^d, \ u\in U.$$

In this section, we assume that \Hyp{0}-\Hyp{4} are satisfied and that  $f$ and $\ell$ are Lipschitz continuous with respect to the control variable. The approximations $F_k$ and $L_k$ are also assumed to be  Lipschitz continuous:
%3mm}
\begin{eqnarray*}
& &  \|F_k(x,u)-F_k(x',u')\| \leq L_{F,x} \|x-x'\| + L_{F,u} \|u - u'\|,\\ 
& &  |L_k(x,u)-L_k(x',u')| \leq L_{L,x} \|x-x'\| + L_{L,u} \|u - u'\|,
\end{eqnarray*}
%\MODIFA{
for every $x\in \R^d$ and $u\in U$. Moreover, we assume that the Lipschitz constants of $F_k$ and $L_k$ are related to the Lipschitz constants of $f$ and $\ell$ by the following relations
%3mm}
 \begin{subequations}\label{eq.LFxLFa}
 \begin{eqnarray}
 & &  L_{F,x}  := 1 + \Delta t L_{f,x}C \quad   L_{F,u}  := \Delta t L_{f,u} C \ \ \mbox{and} \\
 & &
  L_{L,x}  := \frac{\Delta t }{2}L_{\ell,x} C,  \quad  L_{L,a}  := \Delta t (L_{\ell,a}+C),
\end{eqnarray}
\end{subequations}
where the constant $C>0$ may depend on $\Delta t$ and the Lipschitz constants of $f$ and $\ell$.

%
Now, we define the  state-constrained optimal control problem
\begin{equation}
\label{422}
    V(x):=\underset{\bu=(u_k)_k\in U^N}{\inf}
     \Bigg\{ \sum_{k=0}^{N-1} L_k(\yxuk,u_k) + \varphi(\yxuN)   \hspace{0.1cm}
     \mid \hspace{0.1cm} g(\yxuk)\leq 0
     \hspace{0.2cm} \forall k=0,...,N, \hspace{0.2cm} g_f(y^{x,\bu}_N)\leq 0 \Bigg\}.
\end{equation}
%
Then, for the discrete auxiliary control problem,
we define the cost functional $J$ by 
\begin{equation}
\label{424}
    J(x,z,\bu) := \Big( \sum_{k=0}^{N-1} L_k(\yxuk,u_k) + \varphi(\yxuN) -z \Big)  \bigvee
    \Big(\underset{0\leq k\leq N}{\max}\hspace{0.1cm} g(\yxuk)\Big)\bigvee g_f(y^{x,\bu}_N)
\end{equation}
(for $(x,z,\bu=(u_k))\in \R^d\times\R\times U^N$)
and the corresponding auxiliary value is defined, for $(x,z)\in \R^d\times\R$, by
\begin{equation}\label{eq:pbaux}
    W(x,z) := \underset{\bu=(u_k)_k\in U^N}{\inf} J(x,z,\bu).
\end{equation}
Notice  that $W(x,z)$ converges to $\WW(0,x,z)$ as $N\to \infty$ (i.e., $\Delta t=\frac{T}{N} \to 0$), where the continuous value function $\WW$ is defined in \eqref{eq:wg}.
Under Assumption \Hyp{4},  the error estimate of $|W(x,z)-\WW(0,x,z)|$ is 
   bounded by $O(\frac{1}{N})$,  see \cite[Appendice B]{Bok-Gam-Zid-2022}. 
  Furthermore,    the sequence of discrete-time optimal trajectories  (for $N\in \N$) provide convergent 
  approximations of optimal trajectories of the continuous problem \eqref{eq:pbaux}, see \cite{ref5}.
%

\begin{rem}
The discrete dynamics and the discrete cost can be obtained by a discretization scheme of the time-continuous function $f$ and $\ell$.

It is worth mentioning that he algorithms that will be presented  in this section allow also  to consider situations where the dynamics $F$ and $L$ are obtained by some statistical models which can be enriched during the computational process. 
\end{rem}

With similar arguments as in the proof of  \eqref{eq:vsharp-wg}, we have
\begin{equation}\label{eq.V-W}
V(x) =\inf\{z \mid W(x,z)\leq 0\}.
\end{equation}
Furthermore, a pair $(\bar y,\bar \bu)$ is optimal for problem \eqref{422}
if and only if it is optimal for problem \eqref{eq:pbaux} with $z=V(x)$.


%%%%
%
%%%\begin{rem}
%%%A classic approach to take into account the constraints in \eqref{422} consists 
%%%in approaching $V(x)$ by a penalized problem as follows:
%%%%%One can wonder the interest to go through the auxiliary problem \eqref{423}rather than to consider a classic penalized problem which  could be defined by:
%%%%2mm}
%%%$$
%%%    V_\eps(x):=\underset{a=(u_k)_k\in U^N}{\inf}J_\eps(x,u),
%%%$$
%%%\\[-4mm]
%%%with $\disp J_\eps(x,a):=\sum_{k=0}^{N-1} L(\yxuk,u_k) + \varphi(\yxuN) +\frac1\epsilon\Big(\sum_{k=0}^{N}  g(\yxuk)^++\varphi(\yxuN)^+\Big).$
%%%The advantage of the penalized problem is that it does not increase the 
%%%dimension of the state variable.  However, the Lipschitz constant of the penalized criterion $J_\eps$ involves a term in $\frac1\eps$ which tends to infinity when the penalization parameter $\eps$ goes to $0$.  
%%%Let us emphasize on that the auxiliary problem involves a cost function whose Lipschitz constant does not explode, which makes this problem more suitable than the
%%%classical penalization approach for using HJB and optimistic methods.
%%%\end{rem}
%

% Now, let us first notice that the Lipschitz continuity of $F_k$ and $L_k$, with respect to the control variable $u$, lead to the  existence of a constant $C>0$, independent of $N$, such that for any $(x,z)\in \mathbb{R}^d\times \mathbb{R}$ and $\bu=(u_k)_k, \bar \bu=(\bar u_k)_k \in U^N$,
% %6mm}
% \begin{equation}
% \label{433}
%   | J(x,z,\bu)-J(x,z,\bar \bu) |\leq 
%   C \Delta t \sum_{k=0}^{N-1} \left(\frac{1}{L_{F,x}}\right)^k \|u_k-\bar u_k\|. 
% \end{equation}
% This estimate is crucial in the conception of the optimistic algorithm that will be presented here below.
For the sake of simplicity and without loss of generality, we suppose that the control is of dimension $r=1$ and we denote by $D$ its maximal diameter ($\forall a,a'\in A, \|a-a'\|\leq D$). Let us emphasize that the approach can be generalized to control variables with multiple dimensions. 


Planning algorithms are based on the principles of optimistic optimization. In order to minimize the objective function $J$ over the space $U^N$, we refine, in an iterative way the search space into smaller subsets. 
A search space, called node and denoted by $\U_i$ with $i\in \mathbb{N}$, is a Cartesian product of sub-intervals of $U$, i.e., $\U_i:=U_{i,0}\times U_{i,1}\times\cdots\times U_{i,N-1} \subseteq U^N$, where $U_{i,k}$  represents the control interval at time step $k$, for $k=0,\ldots,N-1$. 
%
The collection of nodes will be organized into a tree $\Upsilon$ that will be constructed progressively by expanding the tree nodes. Expanding a node $\U_i$, with $i\in \mathbb{N}$, consists in choosing an interval $U_{i,k}$, for $k=0,\ldots,N-1$, and splitting it uniformly to $M$ sub-intervals where $M>1$ is a parameter of the algorithm. 
The order of expanded nodes and the intervals that have to be split will be chosen in such a way to minimize the cost $J$. 
For now, we introduce some useful notations related to the tree $\Upsilon$:
%%%
\begin{itemize}[align=left, leftmargin=*, itemindent=0mm, nolistsep]
    \item We associate, for any node $\U_i \in \Upsilon$, a sample sequence of controls $\bu_i:=(u_{i,k})_{k=0}^{N-1}\in \U_i$ such that $u_{i,k}$ corresponds to the midpoint of the interval $U_{i,k}$ for any $k=0,\ldots,N-1$.
%     
    \item Denote $d_{i,k}$, for $k=0,...,N-1$, the diameter of the interval $U_{i,k}$ of some node $\U_i\in \Upsilon$. 
    In particular, 
   $$ d_{i,k} = \frac{D}{M^{s_i(k)}}, $$
   where $s_i(k)$ indicates the number of splits needed to obtain the interval $U_{i,k}$ for $k=0,\ldots,N-1$.
    %12mm}
    \item The depth $p_i$ of a node $\U_i$ is  the total number of splits done to obtain this node:
    \begin{equation}
    \label{444}
    p_i:=\sum_{k=0}^{N-1}s_i(k).   
    \end{equation}
    We denote by $\texttt{Depth}(\Upsilon)$ the maximal depth in the tree $\Upsilon$.
    \item A node $\U_i$ is a tree leaf if it has not been expanded. The set of tree leaves is denoted by $\Lambda$. 
    \item Finally, we denote  by $\Lambda_p:=\Big\{\U_i \in \Upsilon \quad s.t. \quad p_i=p\Big\}$ the set of leaves of $\Upsilon$ of depth $p\in \mathbb{N}$.
\end{itemize}

By selecting controls at the intervals centers and by taking $M$ odd, we guarantee that after expanding a node $\U_i$ we generate at least one node $\U_j$ with  $J(x,z,\bu_j) \leq J(x,z,\bu_i).$ Indeed, the middle child $\U_j$
contains the control sequence of $\U_i$.
\begin{prop}
\label{prop441}
By the tree construction, there exists at least a leaf node $\U_i\in \Lambda$  containing an optimal control sequence and satisfying
%
\begin{equation}
    \label{441}
    J(x,z,\bu_i) -\sigma_{i}  \leq W(x,z) \leq J(x,z,\bu_i),
 \end{equation}
 where $\bu_i$ is the sample control sequence in  $\U_i$ and where
%
  \begin{equation}
 \label{442}
     \sigma_{i}:= \Big(\sum_{k=0}^{N-1}\beta_k d_{i,k} \Big) \bigvee  \Big(\sum_{k=0}^{N-1} \gamma_k d_{i,k} \Big),
 \end{equation}
with $\beta_k$ and $\gamma_k$ positive constants only depending on the Lipschitz constants of $F, L, \Phi$ and of $\Psi$.
\end{prop}
%
%
In the optimistic planning algorithms,  at each iteration, one or several  optimistic nodes are chosen and split to get  from each node $M$ children ($M > 1$ is a fixed parameter of the algorithm). 
To expand a node $\U_i$, we choose an interval from $U_{i,0}\times U_{i,1}\times\cdots\times U_{i,N-1}$ and we partition it uniformly to $M$ sub-intervals. If we choose to split the interval $U_{i,k}$, for some $k=0,\ldots,N-1$, then $M$ nodes with will be generated and then the new error term $\sigma^+_i(k)$ is 
\begin{equation*}
    \sigma^+_i(k):= \Big(\sum_{j=0, j\neq k}^{N-1} \beta_j d_{i,j} + \beta_k \frac{d_{i,k}}{M}\Big)\bigvee \Big(\sum_{j=0, j\neq k }^{N-1} \gamma_j d_{i,j} + \gamma_k \frac{d_{i,k}}{M}\Big).
\end{equation*}
\\[-4mm]
Henceforth, in order to minimize the error $\sigma^+_i(k)$, the best choice of the interval to split, $k^*_i$, is given by:
%3mm}
\begin{equation}
    \label{443}
    k^*_i \in \underset{0\leq k \leq N-1 }{\text{argmin}}  \hspace{0.1cm} \sigma^+_i(k).
\end{equation}

The following result gives 
an upper bound on the error term $\sigma_i$, of any node $\U_i\in \Upsilon$.

\begin{prop}
\label{th441}
Assume that the number of split $M>L_{F,x}>1$. 
Let  $\tau:=\Bigl\lceil \frac{\log M}{\log(L_{F,x})} \Bigr\rceil$.
Consider a node $\U_i$ at some depth $p_i=p$. For $p$ large enough, the error $\sigma_i$ (defined in \eqref{442}) is bounded as follows:  
%3mm}
\begin{equation}
    \label{eq:445}
    \sigma_{i}\leq  \delta_p:=\ c_1(N)\,\Delta t\, M^{-\frac{p}{N}},
\end{equation}
where $c_1(N):=C \frac{1}{1 -  \frac{M^{1/\tau}}{L_{F,x}}} M^{q(N)}$, 
  $q(N):= 2 - (N-1) \frac{\tau-2}{2\tau(\tau-1)}$, and 
    $C\geq 0$ are independent of $N$ and $p$.
\end{prop}


Notice that, by definition, we have $\tau\geq 2$, and $L_{F,x}^{-1} M^{1/\tau} \leq 1$.
Hence~\eqref{eq:445} is meaningful only when the strict inequality $L_{F,x}^{-1} M^{1/\tau} < 1$ holds.
Moreover,   $q(N)\leq 2$,   hence $c_1(N)$ is bounded independently of $N$.
%%%%%
\medskip


%
For a given depth $p\in \mathbb{N}$, we define the set of nodes that will be expanded by optimistic planning algorithms:
$$ 
 \Upsilon^*_p:=\{\U_i \in \Upsilon \hspace{0.2cm} \mbox{at depth}\hspace{0.2cm} p \hspace{0.2cm} | \hspace{0.2cm} J(x,z,u_i)-\delta_p\leq W(x,z)\},
$$
where $\delta_p$ is defined as in~\eqref{eq:445}. The set containing all expanded nodes :
$  \Upsilon^*:= \bigcup_{q\geq 0} \Upsilon^*_q
$
is in general smaller than the whole tree $\Upsilon$. The next definition introduces a notion of {\em branching factor} that is a sort of complexity of the algorithm. The precise definition of this notion is given hereafter.
\begin{definition}
\label{def441}
For a given depth $p\in \mathbb{N}$, we define the asymptotic branching factor  $m$, as the smallest real $m\in [1,M]$ such that 
%6mm}
\be
  \exists R\geq 1,\ \forall p\geq 0,\ |\Upsilon^*_p|\leq R m^p.
  \label{eq:m}
\ee
\end{definition}


Now, we will present the rules for refining the search of an optimal control strategy. In the first algorithm, at each iteration,  the node $\U_{i^*}$ minimizing the lower bound ($J(x,z,u_i)-\sigma_i$) will be selected and split to $M$ children. More precisely, we identify an interval  $U_{i^*,k^*_{i^*}}$ whose partition in $M$ sub-intervals will produce the lowest error  $\sigma_{i^*}(k^*_{i^*})$.

\begin{algorithm*}[!hbtp]
\caption{{\bf Optimistic Planning (OP)}
  \label{alg:OP}
}
\begin{algorithmic}[1]
\REQUIRE 
 The number of intervals $N$, the split factor $M$, the maximal number of expanded nodes $I_{\max}$
\STATE 
 Initialize $\Upsilon$ with a root $\U_0:=U^N$ and $n=0$ ($n:=$ number of expanded nodes).
\WHILE{$n < I_{\max}$}
\STATE 
 Select an optimistic node to expand: $\U_{i^*}\in \underset{\U_i\in \Lambda}{\text{argmin}} \hspace{0.1cm} (J(x,z,\bu_i)-\sigma_i)$. 
\STATE 
Select $k^*_{i^*}$, defined in \eqref{443}, the interval to split for the node $\U_{i^*}$.
\STATE 
Update $\Upsilon$ by expanding $\U_{i^*}$ along $k^*_{i^*}$ and adding its $M$ children.
\STATE 
Update $n=n+1$.
\ENDWHILE
\RETURN 
Control sequence $\bu_{i^*}=(u_{i^*,k})_k\in  U^N$ of the node $\U_{i^*} \in \underset{\U_i\in \Lambda}{\text{argmin}}  \hspace{0.1cm}  J(x,z,\bu_i)$.
\end{algorithmic}
\end{algorithm*}
%



\begin{thm}
\label{th442}
Assume that $M>L_{F,x}>1$.
Let $\bu_{i^*}$ and $J(x,z,\bu_{i^*})$ be the output of the OP\ algorithm, 
and let $n\geq 1$ be the corresponding number of expanded nodes.
 For $n$ large enough, we have
  \begin{equation} \label{eq:err-bound-OP}
  0 \leq J(x,z,\bu_{i^*})-W(x,z) \leq \cE_{\texttt{OP}}(N,n):=
  \ 
  \left\{
  \begin{array}{ll}
    c_2(N)\, n^{- \frac{\log(M)}{N \log(m)}} & \mbox{if}\  m>1, \\
    %c_2(N)\, M^{- \frac{n}{NR}} = (\log(n))^{-\frac{\log(M)}{NR}} & \mbox{if}\  m=1,
    c_2(N) (\log(n))^{-\frac{\log(M)}{NR}} & \mbox{if}\  m=1,
  \end{array}
  \right.
\end{equation}
where $c_2(N)>0$ is bounded uniformly with respect to $N$.
%%%
\end{thm}
In Algorithm~\ref{alg:OP}, the number $I_{\max}$ represents a maximal available computational resource. The number of expanded nodes corresponds to the number of iterations,
since at each iteration only one node is expanded. 
Other  optimistic planning methods can be considered.
For instance, the {\em simultaneous optimistic planning} (SOP)  algorithm or {\em simultaneous optimistic planning with multiple steps} (SOPMS) algorithm that 
expand at each iteration several  nodes at every iteration.
%%%


\paragraph{Test 5.} To show the relevance of this approach, we consider a variant of Zermelo problem where a boat targets the set $\mathcal{C}:=\mathbb{B}(0;0.1)$, 
at time $T=1$,  
with minimal fuel consumption. The dynamics is similar to the one considered in \eqref{eq:dyn_zermelo_Unconstrained}.
We consider also two rectangular obstacles with  horizontal and vertical half lengths 
$(r_x,r_y)$. The  first obstacle is centered at $(-2.0,0.5)$   with 
$(r_x,r_y)=(0.4,0.4)$,  and the second obstacle is centered at  $(-2.5,-1)$  with 
        $(r_x,r_y)=(0.2,1)$.
%
To take into account   the pointwise and final state constraints, we define the functions $g$ and $g_f$ by
\begin{equation*}
  g(x):= \Big( 0.4 - \|x-(-2,0.5)\|_{\infty} \Big) \bigvee \min\big( 0.2 - |x_1 +2.5|, 1 - |x_2+1|\big) 
   \quad \mbox{and} \quad g_f(x) := \|x\|_\infty - 0.1. 
\end{equation*}

For a given $N$,  the discrete control problem  becomes:
\begin{equation*}
  \begin{array}{l}
  V(x)={\inf} \Big\{ 
      \frac1N\sum_{k=0}^{N-1} u_{1,k}     \quad \mbox{with} \quad \bu=((u_{1,k},u_{2,k}))_k \in U^N, \\
      \hspace*{3cm}
       g(y^{\bu}_k)\leq 0\ \ \ \mbox{for } k=0,\cdots,N, \quad \mbox{and } \ \  \Psi(y^{\bu}_N)\leq 0\Big\},   
 \end{array}
\end{equation*}
where $(y^\bu_k)_k$ is the discrete state variable, corresponding to the control policy 
$\bu \in U^N$,  and starting at the initial position $x$.
Henceforth, the discrete auxiliary value function is
\begin{equation*}
  W(x,z) = \underset{\bu \in U^N}{\inf} \Big\{ \Big(\frac1N\sum_{k=0}^{N-1}u_{1,k}  -z \Big)  \bigvee
    \Big(\underset{0\leq k\leq N}{\max}\hspace{0.1cm} g(y^{\bu}_k)\Big)\bigvee g_f(y^{\bu}_N) \Big\}.
\end{equation*}

Figure~\ref{fig:zermelo_unconstrained_OP_traj} displays optimal  trajectories obtained  from three different initial positions.
A simultaneous optimistic planning algorithm is used for this simulation with $I_{\max}=3200$ and $N=40$. The optimal controls are displayed on Figure~\ref{fig:zermelo_unconstrained_OP_Co}.
\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{ZermeloOP_40.png} 
     \caption{(Test 5) An optimistic planning approach with  $N=40$. Optimal trajectories corresponding to three different initial data. }\label{fig:zermelo_unconstrained_OP_traj}
\end{figure}


\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{ZermeloOP-40_Controles.png}
     \caption{(Test 5)  An optimistic planning approach with  $N=40$. Control laws corresponding to three different initial data. }\label{fig:zermelo_unconstrained_OP_Co}
\end{figure}


