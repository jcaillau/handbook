% todo: deal with greek letters in verbatim; cite bulirsh et al in section 4
\section{Direct methods: nonlinear programming}\label{sec_direct}
%{\color{red}
%Outline:
%\begin{itemize}
%\item general formulation (including as general as possible constraints)
%\item Goddard and Zermelo examples (+ constraints)---refer to 2.3
%\item basic collocation schemes (midpoint / Crank-Nicolson)
%\item convergence results (Hager {\em al} for collocation methods)
%\item modern optimisation and differentiable programming
%\item a do it yourself direct solver (+ notebooks: \texttt{JuMP, ct})
%\item misc: include example of LMI / SDP solve of convex control (e.g. %CDC paper) (alternative discretisation); direct shooting (Diehl {\em et %al}); with DAE (Gerdts {\em et al}); robustness (?)
%\end{itemize}
%}

\subsection{Principle}
We call \emph{direct methods} all numerical methods consisting of numerically solving the optimal control problem as follows: without applying a priori any first-order necessary condition for optimality, we choose a discretization for the state and for the control, we choose a numerical scheme to discretize the control system (differential equation) and the cost functional (integral quadrature), so that the discretized optimal control problem is expressed as a family of nonlinear optimization problems in finite dimension, indexed by a discretization parameter $N$, of the form
\begin{equation}\label{pboptim}
\min_{Z\in \mathcal{C}} F(Z)
\end{equation}
where $Z=(x_1,\ldots,x_N,u_1,\ldots,u_N)$ and
\begin{equation}\label{defC}
\mathcal{C} = \{ Z\ \vert\  g_i(Z)=0,\ i\in{1,\ldots,r},\quad g_j(Z)\leq 0,\ j\in{r+1,\ldots,m} \} .
\end{equation}
This is a classical optimization problem under constraints in finite dimension, with a dimension being as larger as the discretization is finer. 

Of course, there exist an infinite number of variants to discretize the problem and ending up with a problem of the form \eqref{pboptim}. We discuss hereafter several classes of discretizations. Once this transcription has been done, one can then implement a number of various optimization routines to solve \eqref{pboptim}.

\medskip

Let us first explain hereafter one of the simplest possible discretizations.
Consider the optimal control problem \eqref{eq.Pb_CO} with $t=0$ as initial time.
Consider a subdivision $0=t_0<t_1<\cdots<t_N=T$ of the interval $[0,T]$. Controls are discretized on $U$-valued piecewise constant functions on this subdivision. To discretize the control system, we choose the explicit Euler method: setting $h_i=t_{i+1}-t_i$, we have $x_{i+1}=x_i+h_if(t_i,x_i,u_i)$ for $i=0,\ldots,N-1$. To discretize the integral cost, we choose the left rectangle method (which is equivalent to the explicit Euler method for the augmented system). We obtain the nonlinear programming problem
\begin{equation*}
\begin{split}
& \min C(x_0,\ldots,x_N,u_0,\ldots,u_N), \\
& x_{i+1}=x_i+h_if(t_i,x_i,u_i),\quad u_i\in U,\\
& g(x_i)\leq 0, \qquad i=0,\ldots, N-1,\\
& g_f(x_N)\leq 0.
\end{split}
\end{equation*}

We note that this approach is very soft insofar it is not much sensitive to the model (contrarily to the shooting method, described further): it is very easy to add various constraints to the optimal control problem. This is why direct methods are often privileged when the model is not completely fixed. The resulting numerical simulations often give an interesting feedback that may lead to change or adapt the optimal control model under consideration.

\subsection{Practical numerical implementation}
The numerical implementation of such a nonlinear programming problem is standard and can be done in a number of ways, for instance using a penalty method or a SQP method (sequential quadratic programming) or dual methods (like Uzawa). 
A survey on the use of direct methods in optimal control, with a special interest to applications in aerospace, can be found in \cite{Betts}.


From the point of view of practical implementation, in the last years much progress has been done in the direction of combining automatic differentiation softwares (such as the modeling language \texttt{AMPL}, see \cite{AMPL}, or \texttt{CasADi}, see \cite{Andersson2018}) with expert optimization routines (such as the open-source package \texttt{Ipopt}, see \cite{IPOPT}, providing an interior point optimization algorithm for large-scale differential algebraic systems combined with a filter line-search method). 
With such tools it has become very simple to implement with only few lines of code difficult (nonacademic) optimal control problems, with success and within a reasonable time of computation. Websites such as \texttt{NEOS} (\url{http://www.neos-server.org/neos/solvers/index.html}) propose to launch online such kinds of computation: codes can be written in a modeling language such as \cite{AMPL} (or others) and can be combined with many optimization routines (specialized either for linear problems, nonlinear, mixed, discrete, etc). The advantage of using \texttt{NEOS} is that one has nothing to install on his own machine, and moreover one can test a large number of possible optimization routines.
Note that there exists a large number (open-source or not) of automatic differentiation softwares and of optimization routines, it is however not our aim to provide a list of them. They are easy to find on the web.
Note also that \texttt{AMPL}, which is very easy and friendly to use, is however not free of charge (although the licence is not expensive) and that \texttt{CasADi} offers a very good and efficient free alternative, see \url{https://web.casadi.org}.


\subsection{Variants}
As alluded above, there exist many possible approaches to discretize an optimal control problem, see \cite{Betts} where the important sparsity issues are also discussed. Among those various approaches, we quote the following.

Collocation methods consist of choosing specific points or nodes on every subinterval of a given subdivision of the time interval. Such methods consist of approximating the trajectories and the controls by polynomials on each subinterval. The collocation conditions state that the derivatives of the approximated state match with the dynamics at each node. 

Spectral and pseudospectral methods are another class in which the above nodes are chosen as the zeros of specific polynomials such as Gauss-Legendre or Gauss-Lobatto polynomials. Such polynomials are used as a basis to approximate trajectories and controls in appropriate approximation spaces. 
Since they share nice orthogonality properties, the collocation conditions turn into constraints that are easily tractable for numerical purposes. We refer the reader to \cite{ElnagarKazemi,GRKF,RossFahroo} and to the references therein for more details.

There exist also some probabilistic approaches, such as the method described in \cite{HLPT} which consists of first relaxing the optimal control problem in measure spaces and then of seeking the optimal control as an occupation measure, which is approximated by a finite number of its moments (see  \cite{Lasserre}). This approach relies on algebraic geometry tools and reduces the optimal control problem to some finite dimensional optimization problem involving linear matrix inequalities (LMI).
On this topic involving Sums-of-Square (SoS) considerations, we refer the reader to another chapter of the Handbook, \cite{Lasserre_Handbook}.


\begin{rem}
Direct methods consist of first discretizing and then optimizing, i.e., we apply optimality conditions in a second step, to the discretized model, in contrast to indirect methods that are discussed in the next section, which consist of applying first optimality conditions (the Pontryagin maximum principle) and then of discretizing the resulting boundary value problem.

While the latter method clearly falls in the classical Lax scheme, ``consistency plus stability imply convergence", there is a serious gap there in the direct approaches: to ensure convergence using the Lax scheme, one would a priori need a \emph{uniform} (with respect to $N$) consistency property, which is not true in general because the optimal control problem is an optimization problem in infinite dimension. 
Surprisingly simple examples of divergence are provided in \cite{Hager2000}.
In this same paper, it is shown that convergence is obtained for ``smooth enough" optimal control problems without any final state constraint, discretized with Runge-Kutta methods with positive coefficients. 
We also refer to \cite{BonnansLaurentVarin,SanzSerna} for further comments and considerations on symplectic integrators.
Convergence has also been established for classes of Legendre pseudospectral methods (see \cite{ElnagarKazemi,GRKF,RossFahroo}).
\end{rem}

\subsection{Goddard problem by a direct approach} \label{s32}
To illustrate the method, we present a treatment of the Goddard case presented Section~\ref{s2} using the Crank-Nicolson scheme. This scheme, which is \emph{dual} to the midpoint rule (see \cite{hairer-2006a}), has the advantage of using only gridpoints (contrary to the midpoint scheme). It is very easy to implement and the code below is a basic ``do it yourself" direct solver that can easily be adpated to other problems. We use the nice \texttt{JuMP} interface in \texttt{julia} to define the discretization of the problem and call the celebrated interior point solver \texttt{Ipopt} previously mentioned. The symbolic-numeric framework put forward by \texttt{Julia} allows to efficiently and transparently use AD (automatic differentiation / differentiable programming) and sparse linear algebra (which is important for structured constraints stemming from one-step like methods to discretize the dynamics).
In the spirit of reproducible research \cite{donoho-1995a}, the code itself is available and executable online on the gallery of the \texttt{ct:\,control toolbox} project.\footnote{\href{https://ct.gitlabpages.inria.fr/gallery}%
{ct.gitlabpages.inria.fr/gallery}}
Discretizing the dynamics boils down to the following lines (note that nonlinear expressions encoding the right-hand side are first defined):
\begin{verbatim}
# Dynamics
@NLexpressions(sys, begin
    # D = Cd v^2 exp(-β(r-1))
    D[i = 1:N+1], Cd * v[i]^2 * exp(-β * (r[i] - 1.0))
    # r'= v
    dr[i = 1:N+1], v[i]
    # v' = (Tmax.u-D)/m - 1/r^2
    dv[i = 1:N+1], (Tmax*u[i]-D[i])/m[i] - 1/r[i]^2
    # m' = -b.Tmax.u
    dm[i = 1:N+1], -b*Tmax*u[i]
end)

# Crank-Nicolson scheme
@NLconstraints(sys, begin
    con_dr[i = 1:N], r[i+1] == r[i] + Δt * (dr[i] + dr[i+1])/2.0
    con_dv[i = 1:N], v[i+1] == v[i] + Δt * (dv[i] + dv[i+1])/2.0
    con_dm[i = 1:N], m[i+1] == m[i] + Δt * (dm[i] + dm[i+1])/2.0
end)
\end{verbatim}
Also note that the constraints have been labeled so that the corresponding Lagrange multipliers can be retrieved. They are indeed approximations of the costate of the continuous problem and will be used as such to initialize successfully a subsequent shooting method (see Section~\ref{s4}).
%
A strong benefit of direct methods is that state constraints are very easy to handle. We may for instance add a state constraint on the velocity (note that some other constraints have been added, directly when defining the unknowns of the problem, to improve convergence of the solver; what would be a complication for indirect methods is actually an asset here):
\begin{verbatim}
@variables(sys, begin
    0.0 ≤ Δt                      # time step (unknown as tf is free)
    r[1:N+1] ≥ r0                 # r 
    0 ≤ v[1:N+1] ≤ vmax           # v 
    mf ≤ m[1:N+1] ≤ m0            # m
    0.0 ≤ u[1:N+1] ≤ 1.0          # u
end)
\end{verbatim}
Boundary constraints are obviously added in the same way, and for $N=100$ gridpoints we get the results given on Figures~\ref{fig31} and \ref{fig32}. The resulting optimisation problem solved has about $400$ variables (the state is of dimension $3$ and the control is scalar) and $300$ equality constraints (the discretised dynamics), plus box constraints.
One could use a finer grid (up to the price of a larger problem), but the result we obtain turns to be precise enough to trigger convergence of much more accurate solve by shooting in the next section. The bang-singular-constrained-bang structure of the solution has indeed been captured by the direct solve, and this is essentially all we need to resort to indirect methods to complete the computation.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{fig31}
    \caption{\small Goddard problem: result of the direct code (Crank-Nicolson scheme), states and Lagrange multipliers associated with the equality constraints that discretise the dynamics. These multipliers approximate the adjoint states and will used as such to initialise an indirect method. A boundary arc is observed as the state constraint on the velocity is saturated.}
    \label{fig31}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth,height=0.3\textheight]{fig32}
    \caption{\small Goddard problem: result of the direct code, control values. A structure with four different subarcs is observed. A simple inspection indicates a concatenation of a bang arc ($u \simeq 1$), possibly of a singular arc interior to the control bounds ($u \in (0,1)$), of a boundary arc due to the state constraint on the velocity, and of a final bang arc ($u \simeq 0$). This structure information, of combinatorial nature, is obtained here without any \emph{a priori} knowledge on the solution. It is the key to define an appropriate shooting function and solve very accurately the problem thanks to an indirect method.}
    \label{fig32}
\end{figure}